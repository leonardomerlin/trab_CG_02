/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.*;
import java.util.ArrayList;

/**
 *
 * @author Andre Luiz
 */
public class Controle {
    private static Cena cena = new Cena();

    public Controle() {
        cena = new Cena();
    }
  
    public static Cena getCena() {
        return cena;
    }

    public static void setCena(Cena cena) {
        Controle.cena = cena;
    }

    public static void PintarBordasPoligos() {
        Controlador_Paint.PintarBordasPoligonosCena();
    }

    public static void x() {
        Poligono p = new Poligono();
        ArrayList<Aresta> arestas = new ArrayList<>();
        ArrayList<Vertice> pontos = new ArrayList<>();

        Vertice v1 = new Vertice(10, 10, 10);
        Vertice v2 = new Vertice(10, 100, 10);
        Vertice v3 = new Vertice(100, 100, 10);
        Vertice v4 = new Vertice(100, 10, 10);
        Vertice v5 = new Vertice(10, 10, 200);
        Vertice v6 = new Vertice(10, 100, 200);
        Vertice v7 = new Vertice(100, 100, 200);
        Vertice v8 = new Vertice(100, 10, 200);
        pontos.add(v1);
        pontos.add(v2);
        pontos.add(v3);
        pontos.add(v4);
        pontos.add(v5);
        pontos.add(v6);
        pontos.add(v7);
        pontos.add(v8);


        Aresta a1 = new Aresta(v1, v2);
        Aresta a2 = new Aresta(v2, v3);
        Aresta a3 = new Aresta(v3, v4);
        Aresta a4 = new Aresta(v4, v1);
        Aresta a5 = new Aresta(v5, v6);
        Aresta a6 = new Aresta(v6, v7);
        Aresta a7 = new Aresta(v7, v8);
        Aresta a8 = new Aresta(v8, v5);
        Aresta a9 = new Aresta(v1, v5);
        Aresta a10 = new Aresta(v6, v2);
        Aresta a11 = new Aresta(v3, v7);
        Aresta a12 = new Aresta(v4, v8);
        arestas.add(a1);
        arestas.add(a2);
        arestas.add(a3);
        arestas.add(a4);
        arestas.add(a5);
        arestas.add(a6);
        arestas.add(a7);
        arestas.add(a8);
        arestas.add(a9);
        arestas.add(a10);
        arestas.add(a11);
        arestas.add(a12);
        
        p.setArestas(arestas);
        p.setPontos(pontos);

        ArrayList<Aresta> listaArestas = new ArrayList<>();
        listaArestas.add(a1);
        listaArestas.add(a2);
        listaArestas.add(a3);
        listaArestas.add(a4);
        Face f = new Face(listaArestas);
        p.getFaces().add(f);

        listaArestas = new ArrayList<>();
        listaArestas.add(a5);
        listaArestas.add(a6);
        listaArestas.add(a7);
        listaArestas.add(a8);
        f = new Face(listaArestas);
        p.getFaces().add(f);

        listaArestas = new ArrayList<>();
        listaArestas.add(a5);
        listaArestas.add(a10);
        listaArestas.add(a1);
        listaArestas.add(a9);
        f = new Face(listaArestas);
        p.getFaces().add(f);

        listaArestas = new ArrayList<>();
        listaArestas.add(a7);
        listaArestas.add(a12);
        listaArestas.add(a3);
        listaArestas.add(a11);
        f = new Face(listaArestas);
        p.getFaces().add(f);
        p.setIdPoligono(1);
        Controle.getCena().getPoligonos().add(p);

        System.out.println(p.toString());

    }
}
