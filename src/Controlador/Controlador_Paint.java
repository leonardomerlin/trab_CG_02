/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Aresta;
import Modelo.Poligono;
import Modelo.Vertice;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Andre Luiz
 */
public class Controlador_Paint {
    private static Graphics g1;
    private static Graphics g2;
    private static Graphics g3;

    public Controlador_Paint() {
    }

    public Graphics getG1() {
        return g1;
    }

    public static void setG1(Graphics g1) {
        Controlador_Paint.g1 = g1;
    }
    
    public static Graphics getG2() {
        return g2;
    }

    public static void setG2(Graphics g2) {
        Controlador_Paint.g2 = g2;
    }

    public static Graphics getG3() {
        return g3;
    }

    public static void setG3(Graphics g3) {
        Controlador_Paint.g3 = g3;
    }
    
    public static void limpaCena(){
        g1.setColor(Color.WHITE);
        g1.fillRect(0, 0, 340, 255);
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, 340, 255);
        g3.setColor(Color.WHITE);
        g3.fillRect(0, 0, 340, 255);
        
    }
    
    /**
     * Pinta as Linhas de Contorno de todos os Poligonos da Cena
     *
     * @param g componente grafico
     */
    public static void PintarBordasPoligonosCena() {
        // Pinta-se Todos os Poligonos da Cena
        for (int k = 0; k < Controle.getCena().getPoligonos().size(); k++) {
            // pintar as bordas do Poligono
            PintarBordasPoligono(Controle.getCena().getPoligonos().get(k));
        }
    }

    public static void PintarBordasPoligono(Poligono p) {
        //Controle.getG().setColor(p.getCorBorda());
        g1.setColor(p.getCorBorda());
        g2.setColor(p.getCorBorda());
        g3.setColor(p.getCorBorda());
        for (int i = 0; i < p.getFaces().size(); i++) {
            p.getFaces().get(i);
            for (int j = 0; j < p.getFaces().get(i).getListaArestas().size(); j++) {
                pintarAresta(p.getFaces().get(i).getListaArestas().get(j));
                
            }

        }

    }

    public static void pintarAresta(Aresta a) {
        // Arredondando as coordenadas dos vertices da Aresta
        int xi = Math.round(a.getvI().getX());
        int yi = Math.round(a.getvI().getY());
        int xf = Math.round(a.getvF().getX());
        int yf = Math.round(a.getvF().getY());

        // pintar aresta na Vista Frontal
        g1.drawLine(xi, yi, xf, yf);
        
        xi = Math.round(a.getvI().getZ());
        yi = Math.round(a.getvI().getY());
        xf = Math.round(a.getvF().getZ());
        yf = Math.round(a.getvF().getY());

        // pintar aresta na Vista Lateral
        g2.drawLine(xi, yi, xf, yf);
        
        xi = Math.round(a.getvI().getX());
        yi = Math.round(a.getvI().getZ());
        xf = Math.round(a.getvF().getX());
        yf = Math.round(a.getvF().getZ());

        //pintar aresta na Vista Topo
        g3.drawLine(xi, yi, xf, yf);
    }
    
    
    
    public static void destacarPoligonoSelecionado(Poligono pol) {
        for (int i = 0; i < pol.getArestas().size(); i++) {
            destacarVertice(pol.getArestas().get(i).getvI());
        }
    }

    public static void destacarVertice(Vertice v) {
        //Graphics g = area.getGraphics();
        g1.setColor(Color.BLACK);
        g2.setColor(Color.BLACK);
        g3.setColor(Color.BLACK);

        // Arredondando as coordenadas dos vertices da Aresta
        int x = Math.round(v.getX());
        int y = Math.round(v.getY());
        int z = Math.round(v.getZ());
        
        // pintar vertice na Vista Frontal
        g1.drawLine(x, y, x, y);
        //pintar as coordenadas envolta do vertice
        g1.drawLine(x, y + 1, x, y + 1);
        g1.drawLine(x + 1, y, x + 1, y);
        g1.drawLine(x + 1, y + 1, x + 1, y + 1);
        g1.drawLine(x, y - 1, x, y - 1);
        g1.drawLine(x + 1, y - 1, x + 1, y - 1);
        g1.drawLine(x - 1, y, x - 1, y);
        g1.drawLine(x - 1, y - 1, x - 1, y - 1);
        g1.drawLine(x - 1, y + 1, x - 1, y + 1);
        
        
        // pintar vertice na Vista Lateral
        g2.drawLine(z, y, z, y);
        //pintar as coordenadas envolta do vertice
        g2.drawLine(z, y + 1, z, y + 1);
        g2.drawLine(z + 1, y, z + 1, y);
        g2.drawLine(z + 1, y + 1, z + 1, y + 1);
        g2.drawLine(z, y - 1, z, y - 1);
        g2.drawLine(z + 1, y - 1, z + 1, y - 1);
        g2.drawLine(z - 1, y, z - 1, y);
        g2.drawLine(z - 1, y - 1, z - 1, y - 1);
        g2.drawLine(z - 1, y + 1, z - 1, y + 1);
        
        // pintar vertice na Vista de Topo
        g3.drawLine(x, z, x, z);
        //pintar as coordenadas envolta do vertice
        g3.drawLine(x, z + 1, x, z + 1);
        g3.drawLine(x + 1, z, x + 1, z);
        g3.drawLine(x + 1, z + 1, x + 1, z + 1);
        g3.drawLine(x, z - 1, x, z - 1);
        g3.drawLine(x + 1, z - 1, x + 1, z - 1);
        g3.drawLine(x - 1, z, x - 1, z);
        g3.drawLine(x - 1, z - 1, x - 1, z - 1);
        g3.drawLine(x - 1, z + 1, x - 1, z + 1);
    }
}
