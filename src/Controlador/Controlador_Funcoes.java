/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Aresta;
import Modelo.Poligono;
import Modelo.Vertice;

/**
 *
 * @author Andre Luiz
 */
public class Controlador_Funcoes {

    /**
     * Retora o Poligono Selecionado.
     *
     * @param v ponto do usuario
     * @return Retora o Poligono Selecionado
     */
    public static Poligono getPoligonoSelecionado(Vertice v) {
        float distanciaMim = Float.MAX_VALUE;
        Poligono poligonoSelecionado = null;

        for (int i = 0; i < Controle.getCena().getPoligonos().size(); i++) {
            for (int j = 0; j < Controle.getCena().getPoligonos().get(i).getArestas().size(); j++) {
                Aresta reta = Controle.getCena().getPoligonos().get(i).getArestas().get(j);
                // calcula a distancia do ponto até a Reta 
                float d = calcularDistanciaPontoReta(v, reta);
                if (d < distanciaMim) {
                    // atualiza a distancia minima
                    distanciaMim = d;
                    // armazena qual o poligono possui a menor distancia
                    poligonoSelecionado = Controle.getCena().getPoligonos().get(i);
                }
            }
        }
        System.out.println("distancia: " + distanciaMim);// + "  Poligono: " + poligonoSelecionado.getIdPoligono());
        return poligonoSelecionado;
    }

    /**
     * Calcula a distância entre um ponto a uma Reta em um mesmo Plano.
     * Considerando o fato da Reta ser um Seguimento Infinito, onde para cada
     * distancia calculada obtém o seu respectivo ponto de interseção e
     * verifica-se se o mesmo é interno ao segmento de reta. Caso seja retonar o
     * valor da distância calculada. Caso contrario retorna o maior valor
     * possível em float
     *
     * @param v ponto
     * @param reta reta
     * @return Retorna a distância entre um ponto a uma Reta.
     */
    public static float calcularDistanciaPontoReta(Vertice v, Aresta reta) {
        // fazendo a equação da reta do poligono
        double a = reta.getvF().getY() - reta.getvI().getY();//r
        double b = (reta.getvF().getX() - reta.getvI().getX()) * -1;//s
        double c = (reta.getvF().getX() * reta.getvI().getY()) - (reta.getvI().getX() * reta.getvF().getY());// t 

        // fazendo a equação da reta perpendicular atraves da formula: − sx + ry + (sx0 − ry0)=0
        double r2 = b * -1;
        double s2 = a;
        double t2 = (((r2 * -1) * v.getX()) - (s2 * v.getY()));

        //Calculando a distância do ponto a reta atraves da formula: d = |ax + by + c|/ sqrt(a*a + b*b)
        double aux = Math.abs((a * v.getX()) + (b * v.getY()) + c); //d = |ax + by + c|
        aux = (aux / (Math.sqrt((a * a) + (b * b))));  // sqrt (a*a + b*b)
        float d = (float) aux;
        //verifica se está dentro da distacia limite 
        if (d < 8) {
            //encontra o ponto de interseção
            Vertice pontoIntersecao = new Vertice();
            pontoIntersecao.setX((float) (((b * t2) - (s2 * c)) / ((s2 * a) - (b * r2))));
            pontoIntersecao.setY((float) (((c * r2) - (t2 * a)) / ((s2 * a) - (b * r2))));

            // verifica se o ponto é esta dentro do segmento de reta
            if (pontoInternoSeguimentoReta(reta, pontoIntersecao)) {
                return d;
            } else {
                // Caso Contrário verifica se o Ponto esta dentro da distancia limite 
                //entre algum dos extremos da reta.
                // calcula a distancia do ponto ao Vertice inicial da aresta
                d = distanciaPontoPonto(v, reta.getvI());
                if (d < 8) {
                    return d;
                }
                // calcula a distancia do ponto ao Vertice inicial da aresta
                d = distanciaPontoPonto(v, reta.getvF());
                if (d < 8) {
                    return d;
                }
            }
        }
        return Float.MAX_VALUE;
    }

    /**
     * Verifica se um determinado Ponto é Interno (está dentro dos limites) a um
     * Seguimento de Reta
     *
     * @param Aresta r, Seguimento de Reta
     * @param Vertice v Ponto a ser examinado.
     * @return Verdadeiro se o Ponto v é interno. Caso contrário retorna falso.
     */
    public static boolean pontoInternoSeguimentoReta(Aresta r, Vertice v) {
        if (v.getX() >= r.getvI().getX() && v.getX() <= r.getvF().getX()) {
            if (v.getY() >= r.getvI().getY() && v.getY() <= r.getvF().getY()) {
                return true;
            }
            if (v.getY() >= r.getvF().getY() && v.getY() <= r.getvI().getY()) {
                return true;
            }
        }
        if (v.getX() >= r.getvF().getX() && v.getX() <= r.getvI().getX()) {
            if (v.getY() >= r.getvI().getY() && v.getY() <= r.getvF().getY()) {
                return true;
            }
            if (v.getY() >= r.getvF().getY() && v.getY() <= r.getvI().getY()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna a distância entre dois Pontos em um mesmo Plano
     *
     * @param v1 Ponto Inicial
     * @param v2 Ponto Final
     * @return Retorna a distância entre dois Pontos
     */
    public static float distanciaPontoPonto(Vertice v1, Vertice v2) {
        // formula da distacia Raiz((x2-x1)² + (y2-y1)²)
        double dx = (float) Math.pow((v1.getX() - v2.getX()), 2); //(x2-x1)²
        double dy = (float) Math.pow((v1.getY() - v2.getY()), 2); //(y2-y1)²

        return (float) Math.sqrt(dx + dy); //Raiz((x2-x1)² + (y2-y1)²)
    }
    
    
    public static void mover(Poligono p, Vertice vi, Vertice vf){
        int x = (int)(vf.getX() - vi.getX());
        int y = (int)(vf.getY() - vi.getY());
        int z = (int)(vf.getZ() - vi.getZ());
        
        for (int i = 0; i < p.getPontos().size(); i++) {
            p.getPontos().get(i).setX(p.getPontos().get(i).getX()+x);
            p.getPontos().get(i).setY(p.getPontos().get(i).getY()+y);
            p.getPontos().get(i).setZ(p.getPontos().get(i).getZ()+z);
        }
        
    }
    
    public static void escala(Poligono p, float escalar){
        for (int i = 0; i < p.getPontos().size(); i++) {
            p.getPontos().get(i).setX(p.getPontos().get(i).getX()*escalar);
            p.getPontos().get(i).setY(p.getPontos().get(i).getY()*escalar);
            p.getPontos().get(i).setZ(p.getPontos().get(i).getZ()*escalar);
        }
        
    }
    
}
