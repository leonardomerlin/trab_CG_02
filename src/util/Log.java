package util;

import java.util.logging.Logger;

/**
 *
 * @author Leonardo
 */
public class Log {

    private static final Logger log = Logger.getLogger("Main");
    public static void INFO(String msg){
        log.info(msg);
    }
    public static void WARN(String msg){
        log.warning(msg);
    }
    public static void ERROR(String msg){
        log.severe(msg);
    }
}
