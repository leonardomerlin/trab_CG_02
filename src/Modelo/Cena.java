/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author Andre Luiz
 */
public class Cena {
    ArrayList<Poligono> poligonos;

    public Cena() {
        poligonos = new ArrayList<>();
    }

    public Cena(ArrayList<Poligono> poligonos) {
        this.poligonos = poligonos;
    }

    public ArrayList<Poligono> getPoligonos() {
        return poligonos;
    }

    public void setPoligonos(ArrayList<Poligono> poligonos) {
        this.poligonos = poligonos;
    }
    
    
}
