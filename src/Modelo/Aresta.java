package Modelo;

import java.util.Objects;

/**
 *
 * Classe de estrutura e manipulação básica de uma Aresta.
 *
 * Changelog: 25/10/2012 -
 *
 * @author Leonardo Merlin <email: contato@leonardomerlin.com.br> 
 *         + adicionado contrutor com cópia
 *
 * @author Andre Luiz
 */
public class Aresta {

    protected Vertice vI;
    protected Vertice vF;

    /**
     * Construtor default. - Atenção: x1,y1,y1 = x2,y2,z2 = 0;
     */
    public Aresta() {
        vI = new Vertice();
        vF = new Vertice();
    }

    /**
     * Construtor de cópia.
     */
    public Aresta(Aresta _old) {
        //usa-se construtor de cópia dos vértices
        this.vI = new Vertice(_old.vI);
        this.vF = new Vertice(_old.vF);
    }

    public Aresta(Vertice vI, Vertice vF) {
        this.vI = vI;
        this.vF = vF;
    }

    public Vertice getvF() {
        return vF;
    }

    public void setvF(Vertice vF) {
        this.vF = vF;
    }

    public Vertice getvI() {
        return vI;
    }

    public void setvI(Vertice vI) {
        this.vI = vI;
    }

    public Vertice getVerticeMaxY() {
        if (vI.y > vF.y) {
            return vI;
        } else {
            return vF;
        }
    }

    public Vertice getVerticeMinY() {
        if (vI.y < vF.y) {
            return vI;
        } else {
            return vF;
        }
    }

    public Vertice getVerticeMinX() {
        if (vI.x < vF.x) {
            return vI;
        } else {
            return vF;
        }
    }

    public Vertice getVerticeMaxX() {
        if (vI.x > vF.x) {
            return vI;
        } else {
            return vF;
        }
    }

    public Vertice getVerticeMin() {
        if (vI.y != vF.y) {
            return getVerticeMinY();
        } else {
            return getVerticeMinX();
        }
    }

    public Vertice getVerticeMax() {
        if (vI.y != vF.y) {
            return getVerticeMaxY();
        } else {
            return getVerticeMaxX();
        }
    }

    @Override
    public String toString() {
        return "{" + vI.toString() + "," + vF.toString() + "}";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.vI);
        hash = 17 * hash + Objects.hashCode(this.vF);
        return hash;
    }

    /**
     * Cuidado! - Aresta DIRECIONADA!
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aresta other = (Aresta) obj;
        if (!(this.vI.equals(other.vI))) {
            return false;
        }
        if (!(this.vF.equals(other.vF))) {
            return false;
        }
        return true;
    }
}
