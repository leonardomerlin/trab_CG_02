/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author Andre Luiz
 */
public class Face {
    
    boolean scanline;
    ArrayList<Aresta> listaArestas;
    //ArrayList<LinhaPreenchimento> linhasPreenchemento;
    

    public Face() {
        listaArestas = new ArrayList<>();
        //linhasPreenchemento = new ArrayList<>();
    }
    
    public Face(ArrayList<Aresta> listaArestas) {
        this.listaArestas = listaArestas;
        //this.linhasPreenchemento = linhasPreenchemento;
    }

    public ArrayList<Aresta> getListaArestas() {
        return listaArestas;
    }

    public void setListaArestas(ArrayList<Aresta> listaArestas) {
        this.listaArestas = listaArestas;
    }

//    public ArrayList<LinhaPreenchimento> getLinhasPreenchemento() {
//        return linhasPreenchemento;
//    }
//
//    public void setLinhasPreenchemento(ArrayList<LinhaPreenchimento> linhasPreenchemento) {
//        this.linhasPreenchemento = linhasPreenchemento;
//    }

    public boolean isScanline() {
        return scanline;
    }

    public void setScanline(boolean scanline) {
        this.scanline = scanline;
    }
    
    public float getCoordenadaYmax(){
        float yMax = Float.MIN_VALUE;
        
        for (int i = 0; i < listaArestas.size(); i++) {
            if(listaArestas.get(i).getVerticeMaxY().getY()>yMax)
                yMax = listaArestas.get(i).getVerticeMaxY().getY();
        }
        
        return yMax;
    }
    
    public float getCoordenadaYmin(){
        float yMin = Float.MAX_VALUE;
        
        for (int i = 0; i < listaArestas.size(); i++) {
            if(listaArestas.get(i).getVerticeMinY().getY()<yMin)
                yMin = listaArestas.get(i).getVerticeMinY().getY();
        }
        
        return yMin;
    }
    
    /**
     * Verifica se um determinado vertice pertence a de uma ou mais arestas da face 
     * e verifica se o mesmo é o vertice minimo em todas as arestas que percente. 
     * @param v Vertice de comparação
     * @return Verdadeiro se o Vertice v é o vertice minimo em todas as arestas que percente. Caso contrário retonra falso.
     */
    public boolean paridadeVerticeMin(Vertice v, boolean paridadeInicial){
        boolean paridade = paridadeInicial;
        boolean verticeMin = false;
        for (int i = 0; i < listaArestas.size(); i++) {
            // verificar se o vertice perterce a alguma aresta da face
            if(listaArestas.get(i).getVerticeMin().equals(v)){
                verticeMin=true;
                if(paridade)
                    paridade = false;
                else
                    paridade = true;
            }
        }
        if(verticeMin){
            return paridade;
        }
        else
            return false;
    }
    
    public boolean paridadeVerticeMin(Vertice v){
        
        
        for (int i = 0; i < listaArestas.size(); i++) {
            // verificar se o vertice perterce a alguma aresta da face
            if(listaArestas.get(i).getVerticeMin().equals(v)){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Verifica se um determinado vertice pertence a de uma ou mais arestas da face 
     * e verifica se o mesmo é o vertice minimo em todas as arestas que percente. 
     * @param v Vertice de comparação
     * @return Verdadeiro se o Vertice v é o vertice minimo em todas as arestas que percente. Caso contrário retonra falso.
     */
    public boolean isVerticeMinArestas(Vertice v){
        boolean aux = false;
        for (int i = 0; i < listaArestas.size(); i++) {
            // verificar se o vertice perterce a alguma aresta da face
            
            if((listaArestas.get(i).getvI().getX()==v.getX() && listaArestas.get(i).getvI().getY()==v.getY())|| listaArestas.get(i).getvF().getX()==v.getX() && listaArestas.get(i).getvF().getY()==v.getY()){
                // verifica se o vertice é o minimo da aresta
                if(listaArestas.get(i).getVerticeMin().getX()==v.getX() && listaArestas.get(i).getVerticeMin().getY()==v.getY())
                    aux = true;
                else
                    return false;
            }
        }
        return aux;
    }
    
    /**
     * Verifica se um determinado vertice pertence a de uma ou mais arestas da face 
     * e verifica se o mesmo é o vertice maximo em todas as arestas que percente. 
     * @param v Vertice de comparação
     * @return Verdadeiro se o Vertice v é o vertice maximo em todas as arestas que percente. Caso contrário retonra falso.
     */
    public boolean isVerticeMaxArestas(Vertice v){
        boolean aux = false;
        for (int i = 0; i < listaArestas.size(); i++) {
            // verificar se o vertice perterce a alguma aresta da face
            
            if((listaArestas.get(i).getvI().getX()==v.getX() && listaArestas.get(i).getvI().getY()==v.getY())|| listaArestas.get(i).getvF().getX()==v.getX() && listaArestas.get(i).getvF().getY()==v.getY()){
                // verifica se o vertice é o minimo da aresta
                if(listaArestas.get(i).getVerticeMax().getX()==v.getX() && listaArestas.get(i).getVerticeMax().getY()==v.getY())
                    aux = true;
                else
                    return false;
            }
        }
        return aux;
    }

    @Override
    public String toString() {
        String str= "size: "+listaArestas.size();
        for (int i = 0; i < listaArestas.size(); i++) {
            str=str+"\nAresta:"+i+ " vI("+listaArestas.get(i).getvI().getX()+" , "+listaArestas.get(i).getvI().getY()
                    +") vF("+listaArestas.get(i).getvF().getX()+" , "+listaArestas.get(i).getvF().getY()+")";
            
        }
        return str;
    }
    
}
