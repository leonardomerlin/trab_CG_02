/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author Andre Luiz
 */
public class Poligono {
    int idPoligono;
    Color corFundo;
    Color corBorda;
    ArrayList<Face> Faces;
    // listas Auxiliares
    ArrayList<Aresta> Arestas;
    ArrayList<Vertice> pontos;

    public Poligono() {
        Faces = new ArrayList<>();
        corFundo = Color.BLACK;
        corBorda = Color.BLACK;
    }
    
    public Poligono(int idPoligono, ArrayList<Face> faces) {
        this.idPoligono = idPoligono;
        this.Faces = faces;
        corFundo = Color.BLACK;
        corBorda = Color.BLACK;
    }

    public ArrayList<Face> getFaces() {
        return Faces;
    }

    public void setFaces(ArrayList<Face> faces) {
        this.Faces = faces;
    }

    public int getIdPoligono() {
        return idPoligono;
    }

    public void setIdPoligono(int idPoligono) {
        this.idPoligono = idPoligono;
    }

    public Color getCorFundo() {
        return corFundo;
    }

    public void setCorFundo(Color cor) {
        this.corFundo = cor;
    }

    public Color getCorBorda() {
        return corBorda;
    }

    public void setCorBorda(Color corBorda) {
        this.corBorda = corBorda;
    }

    public ArrayList<Aresta> getArestas() {
        return Arestas;
    }

    public void setArestas(ArrayList<Aresta> Arestas) {
        this.Arestas = Arestas;
    }

    public ArrayList<Vertice> getPontos() {
        return pontos;
    }

    public void setPontos(ArrayList<Vertice> pontos) {
        this.pontos = pontos;
    }

    @Override
    public String toString() {
        String str = "Poligono: "+idPoligono;
        for (int i = 0; i < Faces.size(); i++) {
            str +="\nFace: "+ (i+1);
            //Faces.get(i).toString();
            for (int j = 0; j < Faces.get(i).getListaArestas().size(); j++) {
                str +="\nAresta: "+Faces.get(i).getListaArestas().get(j).vI.x;
            }
        }
        return str;
    }
    
    
    
}
