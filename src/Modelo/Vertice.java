package Modelo;

/**
 * 
 * Classe de estrutura e manipulação básica de Vértice.
 *
 * Changelog:
 *     25/10/2012 - @author Leonardo Merlin <email: contato@leonardomerlin.com.br>
 *                  + adicionado contrutor com cópia
 *                  + equals colocado no padrão Java Oracle
 *                  
 * @author Andre Luiz
 */
public class Vertice {
    
    protected float x;
    protected float y;
    protected float z;

    /**
     * Construtor default.
     * x = 0;
     * y = 0;
     * z = 0;
     */
    public Vertice() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    /**
     * Construtor de cópia.
     */
    public Vertice(Vertice _old){
        this.x = _old.x;
        this.y = _old.y;
        this.z = _old.z;
    }

    public Vertice(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Float.floatToIntBits(this.x);
        hash = 97 * hash + Float.floatToIntBits(this.y);
        hash = 97 * hash + Float.floatToIntBits(this.z);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertice other = (Vertice) obj;
        if (Float.floatToIntBits(this.x) != Float.floatToIntBits(other.x)) {
            return false;
        }
        if (Float.floatToIntBits(this.y) != Float.floatToIntBits(other.y)) {
            return false;
        }
        if (Float.floatToIntBits(this.z) != Float.floatToIntBits(other.z)) {
            return false;
        }
        return true;
    }
}
